#include "Matrici.h"
#include <iostream>
using namespace std;

void Afisare(int A[10][10], int n, int m)
{
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
			cout.width(4);
			cout << A[i][j];
		}
		cout << endl;
	}
}

void GenereazaCifreRandom(int A[10][10], int & n, int & m)
{
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
			A[i][j] = rand() % 10;
		}
	}
}

void GenereazaCrescator(int A[10][10], int & n, int & m)
{
	int x = 1;
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
			A[i][j] = x++;
		}
	}
}
